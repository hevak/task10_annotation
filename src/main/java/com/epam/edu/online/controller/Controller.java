package com.epam.edu.online.controller;

public interface Controller {

    void printAnnotationValue();
    void invokeMethods();
    void setValueIntoField();
    void printInformationAboutUser();
}
