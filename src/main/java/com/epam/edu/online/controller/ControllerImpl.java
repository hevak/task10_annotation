package com.epam.edu.online.controller;

import com.epam.edu.online.model.UserReflection;
import com.epam.edu.online.view.MyView;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ControllerImpl implements Controller {

    private final static Logger log = LogManager.getLogger(ControllerImpl.class);
    private UserReflection userReflection;

    public ControllerImpl() {
        this.userReflection = new UserReflection();
    }

    @Override
    public void printAnnotationValue() {
        userReflection.printAnnotationValue();
    }

    @Override
    public void invokeMethods() {
        userReflection.invokeMethods();
    }

    @Override
    public void setValueIntoField() {
        log.info("Enter value");
        String value = MyView.scanner.next();
        userReflection.setValueIntoField(value);
    }

    @Override
    public void printInformationAboutUser() {
        userReflection.showAllInformation();
    }
}
