package com.epam.edu.online.model;

public interface Printable {
    void print();
}
