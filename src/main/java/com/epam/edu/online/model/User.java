package com.epam.edu.online.model;

@MyAnnotation
public class User {

    private String username;
    private String password;
    private int age;

    public User(String username) {
        this.username = username;
    }

    public void setPasswordAndAge(String password, int age) {
        this.password = password;
        this.age = age;
    }
    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public int getAge() {
        return age;
    }

    @Override
    public String toString() {
        return "User{" +
                "username='" + username +
                ", password='" + password + '\'' +
                ", age=" + age +'\'' +
                '}';
    }
}
