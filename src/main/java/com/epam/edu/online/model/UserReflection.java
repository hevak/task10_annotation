package com.epam.edu.online.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.annotation.Annotation;
import java.lang.reflect.*;
import java.util.Random;

public class UserReflection {
    private final static Logger log = LogManager.getLogger(UserReflection.class);

    private Class<User> userClass;
    private User user;
    private MyAnnotation annotation;

    public UserReflection() {
        user = new User("JohnDoe");
        log.info(user);
        userClass = User.class;
        annotation = userClass.getAnnotation(MyAnnotation.class);
        if (!userClass.isAnnotationPresent(MyAnnotation.class)) {
            log.warn("no annotation");
        } else {
            log.info("class annotated - " + annotation);
        }
        log.info(user);

    }

    public void printAnnotationValue() {
        log.info(annotation.toString());
    }

    public void invokeMethods() {
        String string = "first param";
        int integer = new Random().nextInt(5);
        for (Method method : userClass.getDeclaredMethods()) {
            log.info(method.getName());
            log.info("\t" + method.getReturnType());
            if (method.getParameterCount() > 0) {
                try {
                    if (method.getParameterCount() == 1) {
                        log.info("method.invoke - " + method.invoke(user, string));
                    }
                    if (method.getParameterCount() == 2) {
                        log.info("method.invoke - " + method.invoke(user, string, integer));
                    }
                } catch (IllegalAccessException e) {
                    log.error(e.getMessage());
                } catch (InvocationTargetException e) {
                    log.error(e.getMessage());
                }
                methodInformation(method);
            } else {
                try {
                    log.info("method.invoke - " + method.invoke(user));
                } catch (IllegalAccessException e) {
                    log.error(e.getMessage());
                } catch (InvocationTargetException e) {
                    log.error(e.getMessage());
                }
                log.info("\twithout parameters");
            }
        }
    }

    private void methodInformation(Method method) {
        Class<?>[] parameterTypes = method.getParameterTypes();
        for (Class aClass : parameterTypes) {
            log.info("\t" + aClass.getSimpleName());
        }
        Parameter[] parameters = method.getParameters();
        for (Parameter parameter : parameters) {
            log.info("\t" + parameter);
        }
    }

    public void setValueIntoField(String value) {
        for (Field field : userClass.getDeclaredFields()) {
            field.setAccessible(true);
            if (field.getType() == String.class) {
                log.info(field.getName());
                try {
                    field.set(user, value);
                    log.info(field.get(user));
                } catch (IllegalAccessException e) {
                    log.error(e.getMessage());
                }
            }
        }
    }

    public void showAllInformation() {
        printFields();
        printMethods();
        printConstructors();
        printAnnotations();
    }

    private void printAnnotations() {
        log.info("\nannotations");
        for (Annotation annotation : userClass.getDeclaredAnnotations()) {
            log.info(annotation.toString());
        }
    }
    private void printConstructors() {
        log.info("\nconstructors");
        for (Constructor constructor : userClass.getDeclaredConstructors()) {
            log.info(constructor.getName());
            for (Parameter parameter : constructor.getParameters()) {
                printParamInfo(parameter);
            }
        }
    }

            private void printMethods() {
        log.info("\nmethods");
        for (Method method: userClass.getDeclaredMethods()) {
            method.setAccessible(true);
            log.info(method.getName());
            log.info(method.getModifiers());
            log.info(method.getReturnType().getSimpleName());
            log.info(method.getExceptionTypes());
            for (Parameter parameter:method.getParameters()) {
                printParamInfo(parameter);
            }
        }
    }

    private void printParamInfo(Parameter parameter) {
        log.info(parameter.getName());
        log.info(parameter.getType());
    }

    private void printFields() {
        log.info("\nfields");
        for (Field field : userClass.getDeclaredFields()) {
            field.setAccessible(true);
            log.info(field.getName());
            log.info(field.getModifiers());
            log.info(field.getType());
        }
    }


}
