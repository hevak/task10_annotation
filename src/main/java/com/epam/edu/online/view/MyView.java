package com.epam.edu.online.view;

import com.epam.edu.online.controller.Controller;
import com.epam.edu.online.controller.ControllerImpl;
import com.epam.edu.online.model.Printable;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Scanner;

public class MyView {
    private final static Logger log = LogManager.getLogger(MyView.class);

    public static Scanner scanner = new Scanner(System.in);
    private Map<String, String> menu = new LinkedHashMap<>();
    private Map<String, Printable> methodMenu = new LinkedHashMap<>();
    private Controller controller = new ControllerImpl();
    private ResourceBundle bundle;

    public MyView() {
        bundle = ResourceBundle.getBundle("menu");
        setMenu();
        setMenuMethods();
    }

    private void setMenuMethods() {
        methodMenu.put("1", () -> controller.printAnnotationValue());
        methodMenu.put("2", () -> controller.invokeMethods());
        methodMenu.put("3", () -> controller.setValueIntoField());
        methodMenu.put("4", () -> controller.printInformationAboutUser());
    }

    private void setMenu(){
        menu = new LinkedHashMap<>();
        menu.put("1", bundle.getString("1"));
        menu.put("2", bundle.getString("2"));
        menu.put("3", bundle.getString("3"));
        menu.put("4", bundle.getString("4"));
    }

    private void printMenu() {
        menu.forEach((key, value) -> System.out.println(key + "\t - " + value));
        log.info("Enter number: ");
    }

    public void run(){
        String input = "";
        printMenu();
        do {
            try {
                input = MyView.scanner.next().toLowerCase();
                methodMenu.get(input).print();
            } catch (NullPointerException e) {
                log.error("wrongcommand");
                printMenu();
            } catch (Exception e) {
                log.error(e.getMessage());
            }
        } while (!input.equalsIgnoreCase("Q"));
    }


}
